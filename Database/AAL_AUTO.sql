Create database AAL
GO
USE AAL
GO
CREATE TABLE USERS
(
	U_ID INT PRIMARY KEY IDENTITY(1,1),
	USERNAME NVARCHAR(100),
	PASSWORDS NVARCHAR(100),
	ROLES NVARCHAR(20)
)
GO
CREATE TABLE CUSTOMER
(
	C_ID INT PRIMARY KEY IDENTITY(1,1),
	U_ID INT,
	CUSTOMER_NAME NVARCHAR(100),
	CUSTOMER_ADDRESS NVARCHAR(200),
	PHONENUMBER INT,
	EMAIL NVARCHAR(100),
	PASSPORT NVARCHAR(100),
	BALANCES DECIMAL(15,4) NOT NULL DEFAULT '0.0000',
	RATE INT,
	CONSTRAINT FK_CUSTOMER_USERS FOREIGN KEY (U_ID) REFERENCES  USERS(U_ID)
)
GO
CREATE TABLE CATEGORY
(
	CATE_ID INT PRIMARY KEY IDENTITY(1,1),
	CATEGORY_NAME NVARCHAR(200)
)
GO
CREATE TABLE PRODUCT
(
	P_ID INT PRIMARY KEY IDENTITY(1,1),
	PRODUCT_NAME NVARCHAR(200),
	F_ID INT,
	CATE_ID INT,
	IMAGE_LINK NVARCHAR(300),
	PRODUCT_PRICE DECIMAL(15,4) NOT NULL DEFAULT '0.0000',
	QUANTITY INT,
	FEE INT,
	CONSTRAINT FK_PRODUCT_CATEGORY FOREIGN KEY (CATE_ID) REFERENCES  CATEGORY(CATE_ID)
)
GO
CREATE TABLE PRODUCTDETAILS
(
	PD_ID INT PRIMARY KEY IDENTITY(1,1),
	QUALITY NVARCHAR(200),
	CERTIFY NVARCHAR(200),
	DESCRIPTIONS NVARCHAR(200),
	P_ID INT,
	CONSTRAINT FK_PRODUCTDETAILS_PRODUCT FOREIGN KEY (P_ID) REFERENCES  PRODUCT(P_ID)
)
GO
CREATE TABLE ORDERS
(
	O_ID INT PRIMARY KEY IDENTITY(1,1),
	ORDER_IDENTIFIER NVARCHAR(100),
	C_ID INT,
	ORDER_ADDRESS NVARCHAR(100),
	ORDER_DATE DATETIME,
	ORDER_STATUS NVARCHAR(20),
	ORDER_NOTE NVARCHAR(200),
	CONSTRAINT FK_ORDERS_CUSTOMER FOREIGN KEY (C_ID) REFERENCES  CUSTOMER(C_ID)
)
GO
CREATE TABLE ORDERS_HISTORY
(
	HISTORY_ID INT PRIMARY KEY IDENTITY(1,1),
	C_ID INT,
	P_ID INT,
	AMOUNT DECIMAL(15,4) NOT NULL DEFAULT '0.0000',
	QUANTITY INT,
	ORDER_DATE DATETIME,
	ORDER_STATUS NVARCHAR(20),
	CONSTRAINT FK_ORDERSHISTORY_CUSTOMER FOREIGN KEY (C_ID) REFERENCES  CUSTOMER(C_ID),
	CONSTRAINT FK_ORDERSHISTORY_PRODUCT FOREIGN KEY (P_ID) REFERENCES  PRODUCT(P_ID)
)
GO
CREATE TABLE ORDER_PRODUCT
(
	ORDER_PRODUCT_ID INT PRIMARY KEY IDENTITY(1,1),
	O_ID INT,
	P_ID INT,
	QUANTITY INT,
	CONSTRAINT FK_ORDER_PRODUCT_PRODUCT FOREIGN KEY (P_ID) REFERENCES  PRODUCT(P_ID),
	CONSTRAINT FK_ORDER_PRODUCT_ORDERS FOREIGN KEY (O_ID) REFERENCES  ORDERS(O_ID)
)
GO
CREATE TABLE WAREHOUSE
(
	W_ID INT PRIMARY KEY IDENTITY(1,1),
	WARE_NAME NVARCHAR(100),
	WARE_ADDRESS NVARCHAR(100)
)
GO
CREATE TABLE WAREHOUSE_PRODUCT
(
	W_PRODUCT_ID INT PRIMARY KEY IDENTITY(1,1),
	P_ID INT,
	W_ID INT,
	WARE_HOUSE_QUANTITY INT,
	CONSTRAINT FK_WAREHOUSE_PRODUCT_PRODUCT FOREIGN KEY (P_ID) REFERENCES  PRODUCT(P_ID),
	CONSTRAINT FK_WAREHOUSE_PRODUCT_WAREHOUSE FOREIGN KEY (W_ID) REFERENCES  WAREHOUSE(W_ID)
)