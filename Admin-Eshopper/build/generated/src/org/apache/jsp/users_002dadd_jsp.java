package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class users_002dadd_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <title>W3.CSS Template</title>\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/w3.css\" />\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/style.css\" />\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Raleway\" />\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css\" />\n");
      out.write("        <meta charset=\"utf-8\" />\n");
      out.write("    </head>\n");
      out.write("    <body class=\"w3-light-grey\">\n");
      out.write("\n");
      out.write("        <!-- New feed table -->\n");
      out.write("        <div class=\"nortification-table\">\n");
      out.write("            <table class=\"w3-table w3-striped w3-white\">\n");
      out.write("                <tr>\n");
      out.write("                    <td><h5>Thông báo</h5></td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                    <td><i class=\"fa fa-user w3-blue w3-padding-tiny\"></i></td>\n");
      out.write("                    <td>Khách hàng tên khôi vừa đăng ký.</td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                    <td><i class=\"fa fa-user w3-blue w3-padding-tiny\"></i></td>\n");
      out.write("                    <td>Khách hàng tên quang vừa đăng ký.</td>\n");
      out.write("                </tr>\n");
      out.write("            </table>\n");
      out.write("        </div>\n");
      out.write("        <!-- New feed button -->\n");
      out.write("        <div class=\"nortification-btn\" id=\"nortification-btn\">\n");
      out.write("            <a class=\"w3-btn w3-blue\" href=\"#a\"><i class=\"fa fa-comment fa-2\"></i>(3)</a>\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("        <!-- Top container -->\n");
      out.write("        <div class=\"w3-container w3-top w3-black w3-large w3-padding\" style=\"z-index:4\">\n");
      out.write("            <button class=\"w3-btn w3-hide-large w3-padding-0 w3-hover-text-grey\" id=\"open_nav_btn\"><i class=\"fa fa-bars\"></i>  Menu</button>\n");
      out.write("            <span class=\"w3-right\">Logo</span>\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("        <!-- Sidenav/menu -->\n");
      out.write("        <nav class=\"w3-sidenav w3-collapse w3-white w3-animate-left w3-card-4\" style=\"z-index:3;width:200px;\" id=\"mySidenav\"><br>\n");
      out.write("            <div class=\"w3-container w3-row\">\n");
      out.write("                <div class=\"w3-col s4\">\n");
      out.write("                    <img src=\"images/img_avatar2.png\" class=\"w3-circle w3-margin-right\" style=\"width:46px\">\n");
      out.write("                </div>\n");
      out.write("                <div class=\"w3-col s8\">\n");
      out.write("                    <span>Xin Chào, <strong>Khôi</strong></span><br>\n");
      out.write("                    <a href=\"#\" class=\"w3-hover-none w3-hover-text-red w3-show-inline-block\"><i class=\"fa fa-envelope\"></i></a>\n");
      out.write("                    <a href=\"#\" class=\"w3-hover-none w3-hover-text-green w3-show-inline-block\"><i class=\"fa fa-user\"></i></a>\n");
      out.write("                    <a href=\"#\" class=\"w3-hover-none w3-hover-text-blue w3-show-inline-block\"><i class=\"fa fa-cog\"></i></a>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("            <hr>\n");
      out.write("            <div class=\"w3-container\">\n");
      out.write("                <h5>Trang Quản Lý</h5>\n");
      out.write("            </div>\n");
      out.write("            <a href=\"index.html\" class=\"w3-padding\"><i class=\"fa fa-eye fa-fw\"></i> Trang chủ</a>\n");
      out.write("            <a href=\"customers.html\" class=\"w3-padding\"><i class=\"fa fa-users fa-fw\"></i> Khách hàng</a>\n");
      out.write("            <a href=\"users.html\" class=\"w3-padding w3-blue\"><i class=\"fa fa-users fa-fw\"></i> Admin</a>\n");
      out.write("            <a href=\"products.html\" class=\"w3-padding\"><i class=\"fa fa-diamond fa-fw\"></i> Hàng hóa</a>\n");
      out.write("            <a href=\"orders.html\" class=\"w3-padding\"><i class=\"fa fa-bell fa-fw\"></i> Order</a>\n");
      out.write("            <a href=\"#\" class=\"w3-padding\"><i class=\"fa fa-bank fa-fw\"></i>  Kho</a>\n");
      out.write("        </nav>\n");
      out.write("\n");
      out.write("\n");
      out.write("        <!-- Overlay effect when opening sidenav on small screens -->\n");
      out.write("        <div class=\"w3-overlay w3-hide-large w3-animate-opacity\" style=\"cursor:pointer\" title=\"close side menu\" id=\"myOverlay\"></div>\n");
      out.write("\n");
      out.write("        <!-- !PAGE CONTENT! -->\n");
      out.write("        <div class=\"w3-main\" style=\"margin-left:200px;margin-top:43px;\">\n");
      out.write("\n");
      out.write("            <!-- Header -->\n");
      out.write("            <header class=\"w3-container\" style=\"padding-top:22px\">\n");
      out.write("                <h5><b><i class=\"fa fa-dashboard\"></i> Trang Quản Lý</b></h5>\n");
      out.write("            </header>\n");
      out.write("\n");
      out.write("            <div class=\"w3-row-padding w3-margin-bottom\">\n");
      out.write("                <div class=\"w3-container\">\n");
      out.write("                    <form action=\"adduser\" method=\"post\">\n");
      out.write("                        <p>\n");
      out.write("                            <label>Email </label>\n");
      out.write("                            <input class=\"w3-input w3-border w3-padding\" type=\"email\" name=\"email\" value=\"\" placeholder=\"nhập email liên hệ\" />\n");
      out.write("                        </p>\n");
      out.write("                        <p>\n");
      out.write("                            <label>Tên tài khoản: </label>\n");
      out.write("                            <input class=\"w3-input w3-border w3-padding\" type=\"text\" name=\"username\" value=\"\" placeholder=\"nhập tên tài khoản\" />\n");
      out.write("                        </p>\n");
      out.write("                        <p>\n");
      out.write("                            <label>Mật khẩu: </label>\n");
      out.write("                            <input class=\"w3-input w3-border w3-padding\" type=\"password\" name=\"password\" value=\"\" placeholder=\"nhập mật khẩu\" />\n");
      out.write("                        </p>\n");
      out.write("                        <p>\n");
      out.write("                            <label>Nhập lại mật khẩu: </label>\n");
      out.write("                            <input class=\"w3-input w3-border w3-padding\" type=\"password\" name=\"repassword\" value=\"\" placeholder=\"nhập lại mật khẩu\" />\n");
      out.write("                        </p>\n");
      out.write("                        <p>\n");
      out.write("                            <label>Loại admin: </label>\n");
      out.write("                            <select class=\"w3-input w3-border\" name=\"roles\">\n");
      out.write("                                <option  value=\"superadmin\">Super admin</option>\n");
      out.write("                                <option value=\"admin\">Admin</option>\n");
      out.write("                            </select>\n");
      out.write("                        </p>\n");
      out.write("                        <p>\n");
      out.write("                            <button class=\"w3-btn w3-blue\">TẠO</button>\n");
      out.write("                        </p>\n");
      out.write("                    </form>\n");
      out.write("                </div>\n");
      out.write("                <!-- Footer -->\n");
      out.write("                <footer class=\"w3-container w3-padding-16 w3-light-grey\">\n");
      out.write("                    2015 - 2016 Copyright Eshoper\n");
      out.write("                </footer>\n");
      out.write("\n");
      out.write("                <!-- End page content -->\n");
      out.write("            </div>\n");
      out.write("            <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js\"></script>\n");
      out.write("            <script src=\"js/myjs.js\"></script>\n");
      out.write("        </div>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
