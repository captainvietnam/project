
window.onload = function() {
  // Get the Sidenav
  var mySidenav = document.getElementById("mySidenav");

  // Get the DIV with overlay effect
  var overlayBg = document.getElementById("myOverlay");

  // Toggle between showing and hiding the sidenav, and add overlay effect
  function w3_open() {
      if (mySidenav.style.display === 'block') {
          mySidenav.style.display = 'none';
          overlayBg.style.display = "none";
      } else {
          mySidenav.style.display = 'block';
          overlayBg.style.display = "block";
      }
  }

  // Close the sidenav with the close button
  function w3_close() {
      mySidenav.style.display = "none";
      overlayBg.style.display = "none";
  }

  function hide_nortification() {
    $(".nortification-table").css("opacity", "0");
    setTimeout(function() {$(".nortification-table").css("visibility", "hidden");}, 1000);
  }

  function show_nortification() {
      $(".nortification-table").css("visibility", "visible");
      $(".nortification-table").css("opacity", "1");
  }

  function add_nortification() {
    $(".nortification-table tbody").append(
      '<tr> \
        <td><i class="fa fa-user w3-blue w3-padding-tiny"></i></td> \
        <td>New record, over 90 views.</td> \
      </tr> \
    ');
  }

  $("#nortification-btn").click( function() {
    if( $(".nortification-table").css("opacity") == 0 ) {
      show_nortification();
    } else {
      hide_nortification();
    }
  } );

  show_nortification();
  setTimeout(hide_nortification, 3000);
  $("#open_nav_btn").click(w3_open);

}
