/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Cgc_Shyn
 */
@Entity
@Table(name = "warehouse_product")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "WarehouseProduct.findAll", query = "SELECT w FROM WarehouseProduct w"),
    @NamedQuery(name = "WarehouseProduct.findByWProductId", query = "SELECT w FROM WarehouseProduct w WHERE w.wProductId = :wProductId"),
    @NamedQuery(name = "WarehouseProduct.findByWareHouseQuantity", query = "SELECT w FROM WarehouseProduct w WHERE w.wareHouseQuantity = :wareHouseQuantity")})
public class WarehouseProduct implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "W_PRODUCT_ID")
    private Integer wProductId;
    @Column(name = "WARE_HOUSE_QUANTITY")
    private Integer wareHouseQuantity;
    @JoinColumn(name = "P_ID", referencedColumnName = "P_ID")
    @ManyToOne
    private Product pId;
    @JoinColumn(name = "W_ID", referencedColumnName = "W_ID")
    @ManyToOne
    private Warehouse wId;

    public WarehouseProduct() {
    }

    public WarehouseProduct(Integer wProductId) {
        this.wProductId = wProductId;
    }

    public Integer getWProductId() {
        return wProductId;
    }

    public void setWProductId(Integer wProductId) {
        this.wProductId = wProductId;
    }

    public Integer getWareHouseQuantity() {
        return wareHouseQuantity;
    }

    public void setWareHouseQuantity(Integer wareHouseQuantity) {
        this.wareHouseQuantity = wareHouseQuantity;
    }

    public Product getPId() {
        return pId;
    }

    public void setPId(Product pId) {
        this.pId = pId;
    }

    public Warehouse getWId() {
        return wId;
    }

    public void setWId(Warehouse wId) {
        this.wId = wId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (wProductId != null ? wProductId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WarehouseProduct)) {
            return false;
        }
        WarehouseProduct other = (WarehouseProduct) object;
        if ((this.wProductId == null && other.wProductId != null) || (this.wProductId != null && !this.wProductId.equals(other.wProductId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entity.WarehouseProduct[ wProductId=" + wProductId + " ]";
    }
    
}
