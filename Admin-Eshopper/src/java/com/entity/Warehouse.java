/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Cgc_Shyn
 */
@Entity
@Table(name = "warehouse")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Warehouse.findAll", query = "SELECT w FROM Warehouse w"),
    @NamedQuery(name = "Warehouse.findByWId", query = "SELECT w FROM Warehouse w WHERE w.wId = :wId"),
    @NamedQuery(name = "Warehouse.findByWareName", query = "SELECT w FROM Warehouse w WHERE w.wareName = :wareName"),
    @NamedQuery(name = "Warehouse.findByWareAddress", query = "SELECT w FROM Warehouse w WHERE w.wareAddress = :wareAddress")})
public class Warehouse implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "W_ID")
    private Integer wId;
    @Size(max = 100)
    @Column(name = "WARE_NAME")
    private String wareName;
    @Size(max = 100)
    @Column(name = "WARE_ADDRESS")
    private String wareAddress;
    @OneToMany(mappedBy = "wId")
    private List<WarehouseProduct> warehouseProductList;
    @OneToMany(mappedBy = "wId")
    private List<Orders> ordersList;
    @OneToMany(mappedBy = "wId")
    private List<Users> usersList;

    public Warehouse() {
    }

    public Warehouse(Integer wId) {
        this.wId = wId;
    }

    public Integer getWId() {
        return wId;
    }

    public void setWId(Integer wId) {
        this.wId = wId;
    }

    public String getWareName() {
        return wareName;
    }

    public void setWareName(String wareName) {
        this.wareName = wareName;
    }

    public String getWareAddress() {
        return wareAddress;
    }

    public void setWareAddress(String wareAddress) {
        this.wareAddress = wareAddress;
    }

    @XmlTransient
    public List<WarehouseProduct> getWarehouseProductList() {
        return warehouseProductList;
    }

    public void setWarehouseProductList(List<WarehouseProduct> warehouseProductList) {
        this.warehouseProductList = warehouseProductList;
    }

    @XmlTransient
    public List<Orders> getOrdersList() {
        return ordersList;
    }

    public void setOrdersList(List<Orders> ordersList) {
        this.ordersList = ordersList;
    }

    @XmlTransient
    public List<Users> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<Users> usersList) {
        this.usersList = usersList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (wId != null ? wId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Warehouse)) {
            return false;
        }
        Warehouse other = (Warehouse) object;
        if ((this.wId == null && other.wId != null) || (this.wId != null && !this.wId.equals(other.wId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entity.Warehouse[ wId=" + wId + " ]";
    }
    
}
