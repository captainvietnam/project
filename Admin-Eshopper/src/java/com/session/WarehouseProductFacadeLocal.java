/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.session;

import com.entity.WarehouseProduct;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Cgc_Shyn
 */
@Local
public interface WarehouseProductFacadeLocal {

    void create(WarehouseProduct warehouseProduct);

    void edit(WarehouseProduct warehouseProduct);

    void remove(WarehouseProduct warehouseProduct);

    WarehouseProduct find(Object id);

    List<WarehouseProduct> findAll();

    List<WarehouseProduct> findRange(int[] range);

    int count();
    
}
