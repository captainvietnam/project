/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.session;

import com.entity.Users;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Cgc_Shyn
 */
@Stateless
public class UsersFacade extends AbstractFacade<Users> implements UsersFacadeLocal {

    @PersistenceContext(unitName = "Admin-EshopperPU")
    private EntityManager em;
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public UsersFacade() {
        super(Users.class);
    }

    @Override
    public boolean AddUser(String email, String username, String pass, String repass, String role) {
        try {
            Users users = new Users();
            users.setEmail(email);
            users.setUsername(username);
            if (pass.equals(repass)) {
                users.setPasswords(pass);
            } else {
                return false;
            }
            users.setRoles(role);
            em.persist(users);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
